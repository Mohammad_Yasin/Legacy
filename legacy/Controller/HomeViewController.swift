//
//  HomeViewController.swift
//  legacy
//
//  Created by Mohammad yasin on 31/08/20.
//  Copyright © 2020 yasin.MyLegacy. All rights reserved.
//

import UIKit

enum HomeSection: Int {
    case listOfficialMovies
    case listMoviesByGenre

    init(index:Int) {
        switch index {
        case 0:
            self = .listOfficialMovies
        default :
            self = .listMoviesByGenre
        }
    }
}

class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
       
    }

    @IBOutlet weak var tableView: UITableView!
    
    func setupTableView() {
           tableView.delegate = self
           tableView.dataSource = self
           tableView.separatorStyle = .none
           tableView.register(UINib(nibName: "ListOfficialMoviesTableViewCell", bundle: nil), forCellReuseIdentifier: "ListOfficialMoviesTableViewCell")
           tableView.register(UINib(nibName: "ListMoviesByGenreTableViewCell", bundle: nil), forCellReuseIdentifier: "ListMoviesByGenreTableViewCell")
           
//           tableView.contentInset = UIEdgeInsets(top: -50, left: 0, bottom: 0, right: 0)
           tableView.bounces = false
       }
    
}

extension HomeViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         switch HomeSection(index: indexPath.row) {
               case.listOfficialMovies:
                   let cell = tableView.dequeueReusableCell(withIdentifier: "ListOfficialMoviesTableViewCell", for: indexPath) as! ListOfficialMoviesTableViewCell

//                   cell.delegate = self
                   
                   return cell
               case.listMoviesByGenre:
                   let cell = tableView.dequeueReusableCell(withIdentifier: "ListMoviesByGenreTableViewCell", for: indexPath) as! ListMoviesByGenreTableViewCell
                                    
                   return cell
               
               }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 500
        }else {
            return 250
        }
    }
    
}
